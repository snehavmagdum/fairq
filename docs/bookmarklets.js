// Bookmarklets that can be added to Chrome in order to populate forms with data

// Waiting list settings page
javascript:(function () {
    document.querySelector('#description').value = 'Join the waiting list for Garden Zurich West, a vibrant part of the Garden Zurich Club. Your urban gardening adventure starts here, and we can\'t wait for you to join us';
})();

// Applicant page page
javascript:(function () {
    document.querySelector('#first_name').value = 'Alexander';
    document.querySelector('#last_name').value = 'Müdespacher';
    document.querySelector('#birthdate').value = '1987-05-22';
    document.querySelector('#email').value = 'a.muedespacher@gmail.com';
    document.querySelector('#street').value = 'Niederhofenrain 28 A';
    document.querySelector('#phone').value = '+41794415275';
    document.querySelector('#zip').value = '8008';
    document.querySelector('#city').value = 'Zurich';
    document.querySelector('#country').value = 'ch';
    document.querySelector('#org_policy').checked = true;
    document.querySelector('#policy').checked = true;
    document.querySelector('#motivation').value = 'As an avid gardener, I\'m excited for the chance to join the Garden Zurich East community and cultivate my passion in the heart of the city. I look forward to contributing to this green space, learning from fellow gardeners, and promoting urban sustainability.';
})();